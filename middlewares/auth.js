const jwt = require('jsonwebtoken');

module.exports.checkAuth = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decodedToken = jwt.verify(token, 'secret');
        req.userData = { nom: decodedToken.nom, prenom: decodedToken.prenom, userId: decodedToken.id, identifiant: decodedToken.identifiant };
        console.log(req.userData);
        next();
    } catch (error) {
        //  console.log(req.headers);
        res.status(401).json({
            message: 'Auth failed',
            error: error
        });
    }
}
