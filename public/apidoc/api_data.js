define({ "api": [
  {
    "type": "post",
    "url": "/auth/",
    "title": "S'inscrire",
    "group": "Authentification",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Vérifie si utilisateur existe déjà ou non dans la base de données à partir de son nom et de son prénom</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "prenom",
            "description": "<p>Prénom</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mot de passe</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n \"token\": token,\n \"expiresIn\": 3600,\n \"id\": utilisateurTrouve._id,\n \"nom\": utilisateurTrouve.nom,\n \"prenom\": utilisateurTrouve.prenom,\n \"message\": \"successful\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ],
        "401 Non autorisé": [
          {
            "group": "401 Non autorisé",
            "optional": false,
            "field": "Unhautorized",
            "description": "<p>L'utilisateur n'existe pas</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur interne du serveur\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "401 Non autorisé\n{\n  \"message\": \"authentification échouée, utilisateur inexistant\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "401 Non autorisé\n{\n  \"message\": \"Authentification échouée\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/authRoute.js",
    "groupTitle": "Authentification",
    "name": "PostAuth"
  },
  {
    "type": "post",
    "url": "/auth/signIn",
    "title": "Se connecter",
    "group": "Authentification",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Vérifie si le nom, prénom et mot de passe correspondent à un compte utilisateur dans la base de données</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "prenom",
            "description": "<p>Prénom</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"message\": \"User created\",\n    \"id\": result._id,\n    \"nom\": result.nom,\n    \"prenom\": result.prenom\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ],
        "409 Conflit": [
          {
            "group": "409 Conflit",
            "optional": false,
            "field": "Conflict",
            "description": "<p>L'utilisateur créé existe déjà</p>"
          }
        ],
        "400 Mauvaise requête": [
          {
            "group": "400 Mauvaise requête",
            "optional": false,
            "field": "BadRequest",
            "description": "<p>La syntaxe de la requête est erronée</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur interne du serveur\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "409 Erreur interne du serveur\n{\n  \"message\": \"Utilisateur deja existant\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "400 Bad request\n{\n  \"message\": \"Echec de la requete mongoDB\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/authRoute.js",
    "groupTitle": "Authentification",
    "name": "PostAuthSignin"
  },
  {
    "type": "delete",
    "url": "/creneaux/:id",
    "title": "Supprimer un créneau",
    "group": "Creneau",
    "permission": [
      {
        "name": "organisateur"
      }
    ],
    "description": "<p>Permet de supprimer un créneau d'un évènement</p>",
    "success": {
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"message\": \"creneau deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ],
        "401 Non autorisé": [
          {
            "group": "401 Non autorisé",
            "optional": false,
            "field": "Unhautorized",
            "description": "<p>Le créneau n'existe pas</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Créneau introuvable\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Suppression impossible (Participation)\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "401 Non autorisé\n{\n  \"message: \"Not authorized\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/creneauRoute.js",
    "groupTitle": "Creneau",
    "name": "DeleteCreneauxId"
  },
  {
    "type": "get",
    "url": "/evenements/:id",
    "title": "Récupérer les créneaux",
    "group": "Creneau",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Permet de récupérer les créneaux d'un évènement</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "dateDebut",
            "description": "<p>Date de début</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "participant",
            "description": "<p>Nombre de participants autorisés</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "nbParticipant",
            "description": "<p>Nombre de participants présents</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id_\": result._id\n    \"dateDebut\": result.dateDebut\n    \"participant\": result.participant\n    \"nbParticipant\": result.nbParticipant\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Evènement ou créneau introuvable\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/creneauRoute.js",
    "groupTitle": "Creneau",
    "name": "GetEvenementsId"
  },
  {
    "type": "post",
    "url": "/creneaux/",
    "title": "Ajouter un créneau",
    "group": "Creneau",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Permet d'ajouter un créneau à un évènement</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idEvenement",
            "description": "<p>Identifiant de l'évènement contenant ce créneau</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "dateDebut",
            "description": "<p>Date de début</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "201 Created\n{\n    \"id\": result._id\n    \"idEvenement\": result.idEvenement\n    \"dateDebut\": result.dateDebut\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur de creation\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/creneauRoute.js",
    "groupTitle": "Creneau",
    "name": "PostCreneaux"
  },
  {
    "type": "put",
    "url": "/creneaux/:id",
    "title": "Modifier un créneau",
    "group": "Creneau",
    "permission": [
      {
        "name": "organisateur"
      }
    ],
    "description": "<p>Permet de modifier les infromations d'un créneau</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idEvenement",
            "description": "<p>Identifiant de l'évènement contenant ce créneau</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "dateDebut",
            "description": "<p>Nouvelle date de début</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": result._id\n    \"idEvenement\": result.idEvenement\n    \"dateDebut\": result.dateDebut\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Modification impossible\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/creneauRoute.js",
    "groupTitle": "Creneau",
    "name": "PutCreneauxId"
  },
  {
    "type": "delete",
    "url": "/evenements/:id",
    "title": "Supprimer un évènement",
    "group": "Evenement",
    "permission": [
      {
        "name": "organisateur"
      }
    ],
    "description": "<p>Permet de supprimer un évènement</p>",
    "success": {
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"message\": \"Suppression effectuée\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Suppression d'évènement impossible\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Suppression de slot impossible\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Suppression de participation impossible\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/evenementRoute.js",
    "groupTitle": "Evenement",
    "name": "DeleteEvenementsId"
  },
  {
    "type": "get",
    "url": "/evenements/",
    "title": "Récupérer les évènements",
    "group": "Evenement",
    "description": "<p>Permet de récupérer les évènements ainsi que l'utilisateur qui est associé à chaque évènement</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "participant",
            "description": "<p>Nombre de participants</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idCreneauFinal",
            "description": "<p>Identifiant du créneau final</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "etat",
            "description": "<p>Etat (0 =&gt; Récent, 1=&gt;En cours/terminé 2=&gt;Clôturé)</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "organisateur",
            "description": "<p>Objet contenant l'identifiant, le nom et le prénom de l'organisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "duree",
            "description": "<p>Durée</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "creneauFinal",
            "description": "<p>Créneau final</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": evenement._id,\n    \"nom\": evenement.nom,\n    \"participant\": evenement.participant,\n    \"idCreneauFinal\": idCreneauFinal,\n    \"etat\": evenement.etat,\n    \"idUtilisateur\": evenement.idUtilisateur,\n    \"organisateur\": {\n            \"id\": organisateur._id,\n            \"nom\": organisateur.nom,\n            \"prenom\": organisateur.prenom\n    },\n    \"duree\": evenement.duree,\n    \"description\": evenement.description,\n    \"creneauFinal\": evenement.slotFinal\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur de requete des evenements\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/evenementRoute.js",
    "groupTitle": "Evenement",
    "name": "GetEvenements"
  },
  {
    "type": "get",
    "url": "/evenements/:id",
    "title": "Récupérer un évènement",
    "group": "Evenement",
    "description": "<p>Permet de retourner un évènement en fonction de son id ainsi que l'utilisateur qui est associé</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "participant",
            "description": "<p>Nombre de participants</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idCreneauFinal",
            "description": "<p>Identifiant du créneau final</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "etat",
            "description": "<p>Etat (0 =&gt; Récent, 1=&gt;En cours/terminé 2=&gt;Clôturé)</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "organisateur",
            "description": "<p>Objet contenant l'identifiant, le nom et le prénom de l'organisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "duree",
            "description": "<p>Durée</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "creneauFinal",
            "description": "<p>Créneau final</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": evenement._id,\n    \"nom\": evenement.nom,\n    \"participant\": evenement.participant,\n    \"idCreneauFinal\": idCreneauFinal,\n    \"etat\": evenement.etat,\n    \"idUtilisateur\": evenement.idUtilisateur,\n    \"organisateur\": {\n            \"id\": organisateur._id,\n            \"nom\": organisateur.nom,\n            \"prenom\": organisateur.prenom\n    },\n    \"duree\": evenement.duree,\n    \"description\": evenement.description,\n    \"creneauFinal\": evenement.slotFinal\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur de requete des evenements\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/evenementRoute.js",
    "groupTitle": "Evenement",
    "name": "GetEvenementsId"
  },
  {
    "type": "post",
    "url": "/evnements/",
    "title": "Ajouter un évènement",
    "group": "Evenement",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Permet d'ajouter un évènement</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "duree",
            "description": "<p>Durée</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "participant",
            "description": "<p>Nombre de participants</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "etat",
            "description": "<p>Etat (0 =&gt; Récent, 1=&gt;En cours/terminé 2=&gt;Clôturé)</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "201 Created\n{\n    \"id\": result._id,\n    \"nom\": evenement.nom,\n    \"duree\": evenement.duree,\n    \"participant\": evenement.participant,\n    \"etat\": evenement.etat,\n    \"idUtilisateur\": evenement.idUtilisateur,\n    \"description\": evenement.description\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur d'insertion des données\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/evenementRoute.js",
    "groupTitle": "Evenement",
    "name": "PostEvnements"
  },
  {
    "type": "put",
    "url": "/evenements/:id",
    "title": "Modifier un évènement",
    "group": "Evenement",
    "permission": [
      {
        "name": "organisateur"
      }
    ],
    "description": "<p>Permet de modifier les infromations d'un évènement</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nouveau nom</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "duree",
            "description": "<p>Nouvelle durée</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Nouvelle description</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "participant",
            "description": "<p>Nouveau nombre de participants</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "etat",
            "description": "<p>Nouvel état</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"_id\": evenement._id,\n    \"nom\": evenement.nom,\n    \"duree\": evenement.duree,\n    \"description\": evenement.description,\n   \"participant\": evenement.participant,\n    \"etat\": evenement.etat\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Modification impossible\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/evenementRoute.js",
    "groupTitle": "Evenement",
    "name": "PutEvenementsId"
  },
  {
    "type": "put",
    "url": "/notifications/:id",
    "title": "Lire une notification non lue",
    "group": "Notification",
    "description": "<p>Permet de lire une notification non lue</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur recevant cette notification</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "lu",
            "description": "<p>Indique que la notification est lue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"message\": \"Update successful\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401 Non autorisé": [
          {
            "group": "401 Non autorisé",
            "optional": false,
            "field": "Unhautorized",
            "description": "<p>Le créneau n'existe pas</p>"
          }
        ],
        "304 Non modifié": [
          {
            "group": "304 Non modifié",
            "optional": false,
            "field": "NotModified",
            "description": "<p>Document non modifié depuis la dernière requête</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "401 Non autorisé\n{\n  \"message: \"Not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "304 Document non modifié\n{\n  \"message\": \"Echec de la mise à jour de la notification\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/notificationRoute.js",
    "groupTitle": "Notification",
    "name": "PutNotificationsId"
  },
  {
    "type": "put",
    "url": "/notifications/nonLues",
    "title": "Envoyer une notification",
    "group": "Notification",
    "description": "<p>Permet d'envoyer une nouvelle notification</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur recevant la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Contenu de la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "titre",
            "description": "<p>Titre</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "lu",
            "description": "<p>0=&gt;lue, 1=&gt;non lue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": element._id,\n    \"idUtilisateur\": element.idUtilisateur,\n    \"message\": element.message,\n    \"titre\": element.titre,\n    \"lu\": element.lu\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Aucune nouvelle notification\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/notificationRoute.js",
    "groupTitle": "Notification",
    "name": "PutNotificationsNonlues"
  },
  {
    "type": "put",
    "url": "/notifications/nonLues",
    "title": "Envoyer une notification",
    "group": "Notification",
    "description": "<p>Permet d'envoyer une notification aux utilisateurs qui ont participé à un évènement qui vient d'être clôturé</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur recevant la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Contenu de la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "titre",
            "description": "<p>Titre</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "lu",
            "description": "<p>0=&gt;lue, 1=&gt;non lue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": element._id,\n    \"idUtilisateur\": element.idUtilisateur,\n    \"message\": element.message,\n    \"titre\": element.titre,\n    \"lu\": element.lu\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"erreur d'insertion\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur, pas de participant\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/notificationRoute.js",
    "groupTitle": "Notification",
    "name": "PutNotificationsNonlues"
  },
  {
    "type": "put",
    "url": "/notifications/nonLues",
    "title": "Lire les notifications non lues",
    "group": "Notification",
    "description": "<p>Permet de lire toute les notifications non lues</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur recevant cette notification</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "lu",
            "description": "<p>Indique que la notification est lue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"message\": \"Update successful\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401 Non autorisé": [
          {
            "group": "401 Non autorisé",
            "optional": false,
            "field": "Unhautorized",
            "description": "<p>Le créneau n'existe pas</p>"
          }
        ],
        "304 Erreur interne du serveur": [
          {
            "group": "304 Erreur interne du serveur",
            "optional": false,
            "field": "NotModified",
            "description": "<p>Document non modifié depuis la dernière requête</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "401 Non autorisé\n{\n  \"message: \"Not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Réponse (exemple):",
          "content": "304 Document non modifié\n{\n  \"message\": \"Echec de la mise à jour de la notification\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/notificationRoute.js",
    "groupTitle": "Notification",
    "name": "PutNotificationsNonlues"
  },
  {
    "type": "put",
    "url": "/notifications/nonLues",
    "title": "Récupérer une notification",
    "group": "Notification",
    "description": "<p>Permet de récupérer une notification</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur recevant la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Contenu de la notification</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "titre",
            "description": "<p>Titre</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "lu",
            "description": "<p>0=&gt;lue, 1=&gt;non lue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": element._id,\n    \"idUtilisateur\": element.idUtilisateur,\n    \"message\": element.message,\n    \"titre\": element.titre,\n    \"lu\": element.lu\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404 Non trouvé": [
          {
            "group": "404 Non trouvé",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Le serveur n'a pas trouvé la ressource</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "404 Ressource non trouvée\n{\n  \"message\": \"Aucune nouvelle notification\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/notificationRoute.js",
    "groupTitle": "Notification",
    "name": "PutNotificationsNonlues"
  },
  {
    "type": "delete",
    "url": "/participations/:id",
    "title": "Supprimer une participation",
    "group": "Participation",
    "permission": [
      {
        "name": "organisateur"
      }
    ],
    "description": "<p>Permet de supprimer un créneau d'un évènement</p>",
    "success": {
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"_id\": result._id\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"erreur de suppression\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/participationRoute.js",
    "groupTitle": "Participation",
    "name": "DeleteParticipationsId"
  },
  {
    "type": "post",
    "url": "/participations/",
    "title": "Ajouter une participation",
    "group": "Participation",
    "permission": [
      {
        "name": "participant"
      }
    ],
    "description": "<p>Permet d'ajouter une particpation à un créneau</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idUtilisateur",
            "description": "<p>Identifiant de l'utilisateur</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idEvenement",
            "description": "<p>Identifiant de l'évènement</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "idSlot",
            "description": "<p>Identifiant du créneau</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "201 Created\n{\n    \"id\": result._id\n    \"idUtilisateur\": result.idUtilisateur\n    \"idEvenement\": result.idEvenement\n    \"idSlot\": result.idSlot\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur ajout\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/participationRoute.js",
    "groupTitle": "Participation",
    "name": "PostParticipations"
  },
  {
    "type": "get",
    "url": "/utilisateurs/:id",
    "title": "Récupérer un utilisateur",
    "group": "Utilisateur",
    "description": "<p>Permet de récupérer les informations d'un utilisateur par rapport à son identifiant</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "id_",
            "description": "<p>Identifiant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "prenom",
            "description": "<p>Prénom</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de succès",
          "content": "200 OK\n{\n    \"id\": result._id,\n    \"nom\": result.nom,\n    \"prenom\": result.prenom\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500 Erreur interne du serveur": [
          {
            "group": "500 Erreur interne du serveur",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Le serveur a rencontré une erreur interne</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Réponse (exemple):",
          "content": "500 Erreur interne du serveur\n{\n  \"message\": \"Erreur interne du serveur\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/utilisateurRoute.js",
    "groupTitle": "Utilisateur",
    "name": "GetUtilisateursId"
  }
] });
