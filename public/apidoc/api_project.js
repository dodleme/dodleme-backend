define({
  "name": "Dodle-Me",
  "version": "1.0.0",
  "description": "Documentation de l'API",
  "header": {
    "content": "<p>La documentation de l'API est générée par Apidoc. Pour lire correctement le fichier index.html, il faut l'ouvrir depuis le projet car ce fichier fait appel à d'autres fichiers pour se construire.\nCependant, une version en PDF dans le projet mais elle n'est pas aussi bien généré car le formatage n'a pas tout inclut.</p>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-05-30T13:47:39.857Z",
    "url": "http://apidocjs.com",
    "version": "0.22.1"
  }
});
