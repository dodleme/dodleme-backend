const mongoose = require('mongoose');
const Participation = require('../models/participation')
const Creneau = require('../models/slot')
const ObjectId = mongoose.Types.ObjectId;

module.exports.updateCreneau=(req, res, next)=> {
    let idCreneau = req.params.id;
    let dateDebut = req.body.dateDebut;
    Creneau.updateOne({_id: idCreneau}, {dateDebut: dateDebut}).then(result => {
        return res.status(200).json({
            result
        });
    }).catch(error => {
        res.status(500).json({
            message: 'Modification impossible',
            error: error
        });
    });
};

module.exports.addCreneau=(req, res, next)=> {
    let idEvenement  = req.body.slot.idEvenement;
    let dateDebut = req.body.slot.dateDebut;
    const creneau = new Creneau({dateDebut: dateDebut, idEvenement: idEvenement});
    creneau.save().then(resultat => {
        return res.status(201).json({
            id: resultat._id
        });
    }).catch(error => {
        return res.status(500).json({
            message: 'Erreur de creation',
            error: error
        });
    });
};

module.exports.deleteCreneau=(req, res, next)=> {
    let idCreneau = req.params.id;
    Participation.deleteOne({idSlot: idCreneau}).then(result => {
        Creneau.deleteOne({_id: idCreneau}).then((deletedSlot) => {
            if (deletedSlot.n > 0) {
                return res.status(200).json({ message: "creneau deleted" });
            } else {
                return res.status(401).json({ message: "Not authorized"});
            }
        }).catch(error => {
            console.log(error);
            return res.status(500).json({
                message: 'Creneau introuvable',
            });
        })
    }).catch(error => {
        return res.status(500).json({
            message: 'Suppression impossible (Participation)'
        });
    });
};

module.exports.getCreneauxByEv = (req,res, next) => {
    let idParam = req.params.id;
    let listeCreneaux = [];
    Creneau.aggregate([
        {$match: {idEvenement: {$eq: ObjectId(idParam)}  }},
        { $lookup: {
                from: 'participations',
                localField: "_id",

                foreignField: "idSlot",

                as: "participation"
            }
        },
        {

            $unwind: {
                path: "$participation",

                "preserveNullAndEmptyArrays": true
            }
        }
        ,
        {
            $group: {
                _id: "$_id",
                idEvenement: {$first: "$idEvenement"},
                dateDebut: {$first: "$dateDebut"},
                participant: {$push: "$participation"}
            }
        }
    ]).then(result => {
        result.forEach(creneau => {
            listeCreneaux.push({id: creneau._id, dateDebut: creneau.dateDebut, participant: creneau.participant, nbParticipant: creneau.participant.length});
        });
        return res.status(200).json({
            listeCreneaux
        });
    }).catch( err =>{
            return res.status(500).json({
                message: "Evènement ou créneau introuvable"
            });
        }
    );
}
