const Particitipation = require('../models/participation');

module.exports.addParticipation = (req, res, next) => {
    let participation = new Particitipation({idUtilisateur: req.body.idUtilisateur, idEvenement: req.body.idEvenement, idSlot: req.body.idSlot})
    participation.save().then(result => {
        res.status(201).json(result);
    }).catch(err => {
        res.status(500).json(
            {
                error: err,
                message: 'Erreur ajout'
            }
        )
    })
}

module.exports.deleteParticipation = (req, res, next) => {
    let idParticipation = req.params.id;

    Particitipation.deleteOne({_id: idParticipation}).then(result => {
        return res.status(200).json(result);
    }).catch(err => {
        res.status(500).json({
            error: err,
            message: 'erreur de suppression'
        })
    })
}
