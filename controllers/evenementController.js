const mongoose = require('mongoose');
const Evenement = require('../models/evenement');
const Slot = require('../models/slot');
const ObjectId = mongoose.Types.ObjectId;
const Participation = require('../models/participation');

module.exports.addEvenement = (req, res, next) => {
    let evenement = req.body.evenement;
    let evenementCreer = new Evenement({
        nom: evenement.nom,
        duree: evenement.duree,
        participant: evenement.participant,
        idUtilisateur: req.userData.userId,
        etat: 0,
        description: evenement.description
    });
    evenementCreer.save()
        .then(result => {
            return res.status(201).json({
                id: result._id,
                nom: result.nom,
                duree: result.duree,
                participant: result.participant,
                etat: result.etat,
                idUtilisateur: result.idUtilisateur,
                description: result.description
            });
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({
                message: "Erreur d'insertion des données",
                error: err
            })
        })
};

module.exports.deleteEvenement = (req, res, next) => {
    let idEvenement = req.params.id;
    Participation.deleteOne({idEvenement: idEvenement}).then(() =>
        Slot.deleteOne({idEvenement: idEvenement}).then(() =>
            Evenement.deleteOne({_id: idEvenement}).then(() =>
                res.status(200).json({
                    message: 'Suppression effectuée'
                })
            ).catch(error =>
                res.status(500).json({
                    message: 'Suppression d\'évènement impossible'
                })
            )
        ).catch(error =>
            res.status(500).json({
                message: 'Suppression de slot impossible'
            })
        )
    ).catch(error =>
        res.status(500).json({
            message: 'Suppression de participation impossible'
        })
    )

};

module.exports.updateEvenement = (req, res, next) => {
    idEvenement = req.params.id;
    let evenement = req.body.evenement;
    let modification;
    if(evenement.idCreneauFinal !== "") {
        modification = {
            "nom": evenement.nom,
            "duree": evenement.duree,
            "description": evenement.description,
            "participant": evenement.participant,
            "etat": evenement.etat,
            "idCreneauFinal": ObjectId(evenement.idCreneauFinal)
        };
    }else{
        modification = {
            "nom": evenement.nom,
            "duree": evenement.duree,
            "description": evenement.description,
            "participant": evenement.participant,
            "etat": evenement.etat
        }
    }
    Evenement.updateOne({_id: idEvenement}, modification).then(result => {
        console.log(result);
        return res.status(200).json({
            result
        });
    }).catch(error => {
        console.log(error);
        return res.status(500).json({
            message: 'Modification impossible',
            error: error
        });
    });
};

module.exports.getEvenements = (req, res, next) => {
    let listeEvenements = [];
    //Aggregate dispose de propriétés permettant de faire des left joins
    Evenement.aggregate([
        {
            $lookup: {

                from: "utilisateurs",

                localField: "idUtilisateur",

                foreignField: "_id",

                as: "organisateur"

            }
        }
        , {
            $unwind: "$organisateur"
        },{
            $lookup: {
                from: "slots",
                localField: "idCreneauFinal",
                foreignField: "_id",
                as: "slotFinal"
            }
        }, {
            $unwind: {
                "path": "$slotFinal",
                "preserveNullAndEmptyArrays": true
            }
        }

    ]).then(result => {
        console.log(result)
        result.forEach(
            evenement => {
                let idCreneauFinal = "";
                let organisateur = evenement.organisateur
                if (evenement.idCreneauFinal !== "" || evenement.idCreneauFinal !== undefined) {
                    idCreneauFinal = evenement.idCreneauFinal

                }
                listeEvenements.push({
                    id: evenement._id,
                    nom: evenement.nom,
                    participant: evenement.participant,
                    idCreneauFinal: idCreneauFinal,
                    etat: evenement.etat,
                    idUtilisateur: evenement.idUtilisateur,
                    organisateur: {id: organisateur._id, nom: organisateur.nom, prenom: organisateur.prenom},
                    duree: evenement.duree,
                    description: evenement.description,
                    creneauFinal: evenement.slotFinal
                })
            }
        )
        return res.status(200).json(
            listeEvenements
        );
    }).catch(err => {
        return res.status(500).json({
            error: err,
            message: "Erreur de requete des evenements"
        });
    });

};

module.exports.getEvenement = (req, res, next) => {
    let idParam = req.params.id;
    let listeEvenements = [];
    console.log("ok " + idParam);
    Evenement.aggregate([
        {
            $lookup: {

                from: "utilisateurs",

                localField: "idUtilisateur",

                foreignField: "_id",

                as: "organisateur"

            }
        }, {
            $unwind: "$organisateur"
        },
        {$match: {_id: {$eq: ObjectId(idParam)}}}

    ]).then(result => {

        result.forEach(
            evenement => {
                let idCreneauFinal = "";
                let organisateur = evenement.organisateur
                if (evenement.idCreneauFinal !== "" || evenement.idCreneauFinal !== undefined) {
                    idCreneauFinal = evenement.idCreneauFinal
                }
                listeEvenements.push({
                    id: evenement._id,
                    nom: evenement.nom,
                    participant: evenement.participant,
                    idCreneauFinal: idCreneauFinal,
                    description: evenement.description,
                    etat: evenement.etat,
                    idUtilisateur: evenement.idUtilisateur,
                    organisateur: {id: organisateur._id, nom: organisateur.nom, prenom: organisateur.prenom, identifiant: organisateur.identifiant},
                    duree: evenement.duree
                })
            }
        )
        return res.status(200).json(
            listeEvenements
        );
    }).catch(err => {
        return res.status(500).json({
            error: err,
            message: "Erreur de requête des évènements"
        });
    });
}
