const mongoose = require('mongoose');
const Utilisateur = require('../models/utilisateur');
const bCrypt = require('bcrypt'); // module qui sert à hasher les mots de passe
const jwt = require('jsonwebtoken');

module.exports.getUtilisateurs = (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    Utilisateur.find().then(listeUtilisateurs => {
        res.status(200).json({
            message: 'Successful',
            listeUtilisateurs: listeUtilisateurs
        });
    }).catch(err => {
            res.status(400).json({
                message: 'Echec de la requete mongoDB'
            });
        }
    );
}

module.exports.ajouterUtilisateur = (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    let nom = req.body.nom;
    let prenom = req.body.prenom;
    let password = req.body.password;
    let identifiant = req.body.identifiant;

    //Hashage du password, si ce dernier ce passe bien on continue le processus normal en callback de du hashage
    bCrypt.hash(password, 10).then(passwordHashed => {
            const utilisateur = new Utilisateur({identifiant: identifiant, nom: nom, prenom: prenom, password: passwordHashed});
            utilisateur.save().then(result => {
                return res.status(201).json({
                    message: 'User created',
                    id: result._id,
                    nom: result.nom,
                    prenom: result.prenom
                });
            })
                .catch(err => {
                    console.log(err);
                    return res.status(400).json({
                        message: 'Utilisateur deja existant ou autre erreur'
                    });
                })
        }
    ).catch(err => {
        console.log(err)

        return res.status(500).json({
            message: 'Erreur du serveur'
        });
    });
};

module.exports.signIn = (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    //initialisation
    let utilisateurTrouve;
    let identifiant = req.body.identifiant;
    let password = req.body.password;
    //Recherche de correspondance d'utilisateur
    Utilisateur.findOne({identifiant: identifiant}).then(utilisateur => {
        console.log(utilisateur)
        if (!utilisateur) {
            //cas où l'utilisateur n'existe pas dans la base
            return res.status(401).json({
                message: "authentification échouée, utilisateur inexistant"
            })
        }
        utilisateurTrouve = utilisateur;
        console.log(utilisateur)

        return bCrypt.compare(password, utilisateur.password);
    }).then(result => {
        console.log(result)
        if (!result) {
            //401: code d'erreur sur les échecs d'authentification
            return res.status(401).json({
                message: "Authentification échouée"
            });
        }
        //Creation du token jwt avec une expiration d'une heure
        let token = jwt.sign({
                id: utilisateurTrouve._id,
                prenom: utilisateurTrouve.prenom,
                nom: utilisateurTrouve.nom,
                identifiant: utilisateurTrouve.identifiant
            },
            'secret',
            {expiresIn: '1h'}
        );
        console.log(token);
        return res.status(200).json({
            token: token,

            expiresIn: 3600,
            id: utilisateurTrouve._id,
            nom: utilisateurTrouve.nom,
            prenom: utilisateurTrouve.prenom,
            identifiant: utilisateurTrouve.identifiant,
            message: 'successful'
        });


    }).catch(err => {
        return res.status(500).json({
            message: "Erreur du serveur",
            error: err
        })
    });
}

module.exports.getUtilisateurById = (req, res, next) => {
    let id = req.params.id;
    Utilisateur.findById(id).then(utilisateur => {
        res.status(200).json({
            nom: utilisateur.nom,
            prenom: utilisateur.prenom,
            identifiant: utilisateur.identifiant,
            id: utilisateur.id
        });
    }).catch(err => {
        res.status(500).json({
            error: err,
            message: "Erreur interne du serveur"
        })
    });
}
