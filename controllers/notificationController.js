const mongoose = require('mongoose');
const Notification = require('../models/notification');
const Slot= require('../models/slot');
const ObjectId = mongoose.Types.ObjectId;

module.exports.sendNotification=(req, res, next)=> {
    let evId = req.body.id;
    let evenementName = req.body.evName;
    let notifications = [];
    //Recuperation des utilisateurs participant à l'evenement
    Slot.aggregate(
        [{ $match: { idEvenement: {$eq: ObjectId(evId)} } },
            {
                $lookup: {
                    from: "participations",
                    localField: "_id",
                    foreignField: "idSlot",
                    as: "participation"
                }
            },
            {$unwind: "$participation"},
            {
                $lookup: {
                    from: "utilisateurs",
                    localField: "participation.idUtilisateur",
                    foreignField: "_id",
                    as: "utilisateur"
                }
            },
            {$unwind: "$utilisateur"},
            {$group: {
                _id: "$_id",
                    participants: {$push: "$utilisateur"}
            }}
        ]
    ).then(resultat => {
            console.log(resultat[0]);


           resultat[0].participants.forEach(element => {
               let titre = "Cloture de l'evenement " + evenementName;
                let message = "Bonjour " + element.nom + ", l'evenement " + evenementName + " a été cloturé"
                notifications.push({ idEvenement: evId, idUtilisateur: element._id, message: message, lu: false, titre: titre });
            });

            Notification.insertMany(notifications).then(insertResult => {
                let notificationsList = [];
                insertResult.forEach(el => {
                    notificationsList.push({ id: el._id, idUtilisateur: el.idUtilisateur, message: el.message, titre: el.titre });
                })
                console.log(notificationsList);
                res.status(200).json(notificationsList);
            }).catch(err => {

                res.status(500).json({
                    message: "erreur d'insertion"
                })
            })
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            message: "Erreur, pas de participant"
        })
    });
};

module.exports.getNewNotifications = (req, res, next) => {
    let idUser = req.userData.userId;
    let notifications = [];
    Notification.find({idUtilisateur: idUser, lu: false}).then(resultat => {
        console.log(resultat);
        resultat.forEach(element => {

            notifications.push({id: element._id, idUtilisateur: element.idUtilisateur, message: element.message, titre: element.titre, lu: element.lu})
        });
        console.log(notifications)
        res.status(200).json(notifications);
    }).catch(error => {
        console.log(error)
        res.status(500).json({
            message: "Aucune nouvelle notification"
        })
    });
}

module.exports.getNotifications = (req, res, next) => {
    let idUser = req.userData.userId;
    let notifications = [];
    Notification.find({idUtilisateur: idUser}).then(resultat => {

        resultat.forEach(element => {
            notifications.push({id: element._id, idUtilisateur: element.idUtilisateur, titre: element.titre, message: element.message, lu: element.lu})
        });
        res.status(200).json(notifications);
    }).catch(error => {
        console.log(error);
        res.status(404).json({
            message: "Aucune nouvelle notification"
        })
    });
}

module.exports.updateNotificationsNonLu = (req, res, next)=>{
    let idUser = req.userData.userId;
    Notification.updateMany({idUtilisateur: idUser, lu: false}, { lu: true }).then(result => {
        if (result.n > 0) {
            return res.status(200).json({ message: "Update successful"});
        } else {
            return res.status(401).json({ message: "Not authorized"});
        }
    }).catch(error => {
        res.status(304).json({
            message: 'Echec de la mise à jour de la notification'
        });
    });
}

module.exports.updateNotificationNonLu = (req, res, next)=>{
    let idNotif = req.params.id;
    Notification.updateOne({_id: idNotif}, { lu: true }).then(result => {
        if (result.n > 0) {
            return res.status(200).json({ message: "Update successful"});
        } else {
            return res.status(401).json({ message: "Not authorized"});
        }
    }).catch(error => {
        res.status(304).json({
            message: 'Echec de la mise à jour de la notification'
        });
    });
}
