const express = require('express');
const notificationRoute = express.Router();
const notificationController = require('../controllers/notificationController');

const auth = require('../middlewares/auth');

/**
 * @api {put} /notifications/nonLues Envoyer une notification
 * @apiGroup Notification
 * @apiDescription Permet d'envoyer une nouvelle notification
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur recevant la notification
 * @apiSuccess {String}   message   Contenu de la notification
 * @apiSuccess {String}   titre   Titre
 * @apiSuccess {Boolean}   lu   0=>lue, 1=>non lue
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": element._id,
 *          "idUtilisateur": element.idUtilisateur,
 *          "message": element.message,
 *          "titre": element.titre,
 *          "lu": element.lu
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Aucune nouvelle notification"
 *     }
 */
notificationRoute.get('/nonLues', auth.checkAuth, notificationController.getNewNotifications);

/**
 * @api {put} /notifications/nonLues Envoyer une notification
 * @apiGroup Notification
 * @apiDescription Permet d'envoyer une notification aux utilisateurs qui ont participé à un évènement qui vient d'être clôturé
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur recevant la notification
 * @apiSuccess {String}   message   Contenu de la notification
 * @apiSuccess {String}   titre   Titre
 * @apiSuccess {Boolean}   lu   0=>lue, 1=>non lue
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": element._id,
 *          "idUtilisateur": element.idUtilisateur,
 *          "message": element.message,
 *          "titre": element.titre,
 *          "lu": element.lu
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "erreur d'insertion"
 *     }
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur, pas de participant"
 *     }
 */
notificationRoute.post('/', auth.checkAuth ,notificationController.sendNotification);

/**
 * @api {put} /notifications/nonLues Lire les notifications non lues
 * @apiGroup Notification
 * @apiDescription Permet de lire toute les notifications non lues
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur recevant cette notification
 * @apiSuccess {Boolean}   lu   Indique que la notification est lue
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "message": "Update successful"
 *      }
 * @apiError (401 Non autorisé) Unhautorized Le créneau n'existe pas
 * @apiErrorExample Réponse (exemple):
 *     401 Non autorisé
 *     {
 *       "message: "Not authorized"
 *     }
 * @apiError (304 Erreur interne du serveur) NotModified Document non modifié depuis la dernière requête
 * @apiErrorExample Réponse (exemple):
 *     304 Document non modifié
 *     {
 *       "message": "Echec de la mise à jour de la notification"
 *     }
 */
notificationRoute.put('/nonLues', auth.checkAuth, notificationController.updateNotificationsNonLu);

/**
 * @api {put} /notifications/nonLues Récupérer une notification
 * @apiGroup Notification
 * @apiDescription Permet de récupérer une notification
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur recevant la notification
 * @apiSuccess {String}   message   Contenu de la notification
 * @apiSuccess {String}   titre   Titre
 * @apiSuccess {Boolean}   lu   0=>lue, 1=>non lue
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": element._id,
 *          "idUtilisateur": element.idUtilisateur,
 *          "message": element.message,
 *          "titre": element.titre,
 *          "lu": element.lu
 *      }
 * @apiError (404 Non trouvé) NotFound Le serveur n'a pas trouvé la ressource
 * @apiErrorExample Réponse (exemple):
 *     404 Ressource non trouvée
 *     {
 *       "message": "Aucune nouvelle notification"
 *     }
 */
notificationRoute.get('/', auth.checkAuth, notificationController.getNotifications);

/**
 * @api {put} /notifications/:id Lire une notification non lue
 * @apiGroup Notification
 * @apiDescription Permet de lire une notification non lue
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur recevant cette notification
 * @apiSuccess {Boolean}   lu   Indique que la notification est lue
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "message": "Update successful"
 *      }
 * @apiError (401 Non autorisé) Unhautorized Le créneau n'existe pas
 * @apiErrorExample Réponse (exemple):
 *     401 Non autorisé
 *     {
 *       "message: "Not authorized"
 *     }
 * @apiError (304 Non modifié) NotModified Document non modifié depuis la dernière requête
 * @apiErrorExample Réponse (exemple):
 *     304 Document non modifié
 *     {
 *       "message": "Echec de la mise à jour de la notification"
 *     }
 */
notificationRoute.put('/:id', auth.checkAuth, notificationController.updateNotificationNonLu);

module.exports = notificationRoute;
