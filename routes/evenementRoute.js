const express = require('express');
const evenementRoute = express.Router();
const evenementController = require('../controllers/evenementController');
const auth = require('../middlewares/auth');

/**
 * @api {get} /evenements/ Récupérer les évènements
 * @apiGroup Evenement
 * @apiDescription Permet de récupérer les évènements ainsi que l'utilisateur qui est associé à chaque évènement
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {Int}   participant   Nombre de participants
 * @apiSuccess {ObjectId}   idCreneauFinal   Identifiant du créneau final
 * @apiSuccess {Int}   etat   Etat (0 => Récent, 1=>En cours/terminé 2=>Clôturé)
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur
 * @apiSuccess {Object}   organisateur   Objet contenant l'identifiant, le nom et le prénom de l'organisateur
 * @apiSuccess {Int}   duree   Durée
 * @apiSuccess {String}   description   Description
 * @apiSuccess {String}   creneauFinal   Créneau final
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": evenement._id,
 *          "nom": evenement.nom,
 *          "participant": evenement.participant,
 *          "idCreneauFinal": idCreneauFinal,
 *          "etat": evenement.etat,
 *          "idUtilisateur": evenement.idUtilisateur,
 *          "organisateur": {
 *                  "id": organisateur._id,
 *                  "identifiant": result.identifiant,
 *                  "nom": organisateur.nom,
 *                  "prenom": organisateur.prenom
 *          },
 *          "duree": evenement.duree,
 *          "description": evenement.description,
 *          "creneauFinal": evenement.slotFinal
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur de requete des evenements"
 *     }
 */
evenementRoute.get('/',evenementController.getEvenements);

/**
 * @api {get} /evenements/:id Récupérer un évènement
 * @apiGroup Evenement
 * @apiDescription Permet de retourner un évènement en fonction de son id ainsi que l'utilisateur qui est associé
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {Int}   participant   Nombre de participants
 * @apiSuccess {ObjectId}   idCreneauFinal   Identifiant du créneau final
 * @apiSuccess {Int}   etat   Etat (0 => Récent, 1=>En cours/terminé 2=>Clôturé)
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur
 * @apiSuccess {Object}   organisateur   Objet contenant l'identifiant, le nom et le prénom de l'organisateur
 * @apiSuccess {Int}   duree   Durée
 * @apiSuccess {String}   description   Description
 * @apiSuccess {String}   creneauFinal   Créneau final
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": evenement._id,
 *          "nom": evenement.nom,
 *          "participant": evenement.participant,
 *          "idCreneauFinal": idCreneauFinal,
 *          "etat": evenement.etat,
 *          "idUtilisateur": evenement.idUtilisateur,
 *          "organisateur": {
 *                  "id": organisateur._id,
 *                  "identifiant": result.identifiant,
 *                  "nom": organisateur.nom,
 *                  "prenom": organisateur.prenom
 *          },
 *          "duree": evenement.duree,
 *          "description": evenement.description,
 *          "creneauFinal": evenement.slotFinal
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur de requete des evenements"
 *     }
 */
evenementRoute.get('/:id', evenementController.getEvenement);

/**
 * @api {post} /evnements/ Ajouter un évènement
 * @apiGroup Evenement
 * @apiPermission participant
 * @apiDescription Permet d'ajouter un évènement
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {Int}   duree   Durée
 * @apiSuccess {Int}   participant   Nombre de participants
 * @apiSuccess {Int}   etat   Etat (0 => Récent, 1=>En cours/terminé 2=>Clôturé)
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur
 * @apiSuccess {String}   description   Description
 * @apiSuccessExample {json} Exemple de succès
 *      201 Created
 *      {
 *          "id": result._id,
 *          "nom": evenement.nom,
 *          "duree": evenement.duree,
 *          "participant": evenement.participant,
 *          "etat": evenement.etat,
 *          "idUtilisateur": evenement.idUtilisateur,
 *          "description": evenement.description
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur d'insertion des données"
 *     }
 */
evenementRoute.post('/', auth.checkAuth, evenementController.addEvenement);

/**
 * @api {put} /evenements/:id Modifier un évènement
 * @apiGroup Evenement
 * @apiPermission organisateur
 * @apiDescription Permet de modifier les infromations d'un évènement
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   nom   Nouveau nom
 * @apiSuccess {Int}   duree   Nouvelle durée
 * @apiSuccess {String}   description   Nouvelle description
 * @apiSuccess {Int}   participant   Nouveau nombre de participants
 * @apiSuccess {Int}   etat   Nouvel état
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "_id": evenement._id,
 *          "nom": evenement.nom,
 *          "duree": evenement.duree,
 *          "description": evenement.description,
  *         "participant": evenement.participant,
 *          "etat": evenement.etat
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Modification impossible"
 *     }
 */
evenementRoute.put('/:id', auth.checkAuth, evenementController.updateEvenement);

/**
 * @api {delete} /evenements/:id Supprimer un évènement
 * @apiGroup Evenement
 * @apiPermission organisateur
 * @apiDescription Permet de supprimer un évènement
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "message": "Suppression effectuée"
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Suppression d'évènement impossible"
 *     }
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Suppression de slot impossible"
 *     }
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Suppression de participation impossible"
 *     }
 */
evenementRoute.delete('/:id', auth.checkAuth, evenementController.deleteEvenement);

module.exports = evenementRoute;
