const express = require('express');
const utilisateurRoute = express.Router();
const mongoose = require('mongoose');
const utilisateurController = require('../controllers/utilisateurController')

/**
 * @api {get} /utilisateurs/:id Récupérer un utilisateur
 * @apiGroup Utilisateur
 * @apiDescription Permet de récupérer les informations d'un utilisateur par rapport à son identifiant
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {String}   prenom   Prénom
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": result._id,
 *          "nom": result.nom,
 *          "prenom": result.prenom
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur interne du serveur"
 *     }
 */
utilisateurRoute.get("/:id", utilisateurController.getUtilisateurById);

module.exports= utilisateurRoute;
