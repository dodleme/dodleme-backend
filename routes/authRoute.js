const express = require('express');
const authRoute = express.Router();

const mongoose = require('mongoose');
const utilisateurController = require('../controllers/utilisateurController')

/**
 * @api {post} /auth/ S'inscrire
 * @apiGroup Authentification
 * @apiPermission participant
 * @apiDescription Vérifie si utilisateur existe déjà ou non dans la base de données à partir de son nom et de son prénom
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   identifiant   Pseudonyme
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {String}   prenom   Prénom
 * @apiSuccess {String}   password   Mot de passe
 * @apiSuccessExample {json} Exemple de succès
 *     200 OK
 *     {
 *      "token": token,
 *      "expiresIn": 3600,
 *      "id": utilisateurTrouve._id,
 *      "identifiant": result.identifiant,
 *      "nom": utilisateurTrouve.nom,
 *      "prenom": utilisateurTrouve.prenom,
 *      "message": "successful"
 *     }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur interne du serveur"
 *     }
 * @apiError (401 Non autorisé) Unhautorized L'utilisateur n'existe pas
 * @apiErrorExample Réponse (exemple):
 *     401 Non autorisé
 *     {
 *       "message": "authentification échouée, utilisateur inexistant"
 *     }
 * @apiError (401 Non autorisé) Unhautorized Le nom, le prénom et le mot de passe ne correspondent pas
 * @apiErrorExample Réponse (exemple):
 *     401 Non autorisé
 *     {
 *       "message": "Authentification échouée"
 *     }
 */
authRoute.post("/", utilisateurController.ajouterUtilisateur);

/**
 * @api {post} /auth/signIn Se connecter
 * @apiGroup Authentification
 * @apiPermission participant
 * @apiDescription Vérifie si le nom, prénom et mot de passe correspondent à un compte utilisateur dans la base de données
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {String}   identifiant   Pseudonyme
 * @apiSuccess {String}   nom   Nom
 * @apiSuccess {String}   prenom   Prénom
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "message": "User created",
 *          "id": result._id,
 *          "identifiant": result.identifiant,
 *          "nom": result.nom,
 *          "prenom": result.prenom
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur interne du serveur"
 *     }
 * @apiError (409 Conflit) Conflict L'utilisateur créé existe déjà
 * @apiErrorExample Réponse (exemple):
 *     409 Erreur interne du serveur
 *     {
 *       "message": "Utilisateur deja existant"
 *     }
 * @apiError (400 Mauvaise requête) BadRequest La syntaxe de la requête est erronée
 * @apiErrorExample Réponse (exemple):
 *     400 Bad request
 *     {
 *       "message": "Echec de la requete mongoDB"
 *     }
 */
authRoute.post("/signIn", utilisateurController.signIn);

module.exports = authRoute;
