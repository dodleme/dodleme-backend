const creneauController = require('../controllers/creneauController.js');
const express = require('express');
const creneauRoute = express.Router();

/**
 * @api {put} /creneaux/:id Modifier un créneau
 * @apiGroup Creneau
 * @apiPermission organisateur
 * @apiDescription Permet de modifier les infromations d'un créneau
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idEvenement   Identifiant de l'évènement contenant ce créneau
 * @apiSuccess {Date}   dateDebut   Nouvelle date de début
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id": result._id
 *          "idEvenement": result.idEvenement
 *          "dateDebut": result.dateDebut
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Modification impossible"
 *     }
 */
creneauRoute.put('/:id', creneauController.updateCreneau);

/**
 * @api {post} /creneaux/ Ajouter un créneau
 * @apiGroup Creneau
 * @apiPermission participant
 * @apiDescription Permet d'ajouter un créneau à un évènement
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idEvenement   Identifiant de l'évènement contenant ce créneau
 * @apiSuccess {Date}   dateDebut   Date de début
 * @apiSuccessExample {json} Exemple de succès
 *      201 Created
 *      {
 *          "id": result._id
 *          "idEvenement": result.idEvenement
 *          "dateDebut": result.dateDebut
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur de creation"
 *     }
 */
creneauRoute.post('/', creneauController.addCreneau);

/**
 * @api {delete} /creneaux/:id Supprimer un créneau
 * @apiGroup Creneau
 * @apiPermission organisateur
 * @apiDescription Permet de supprimer un créneau d'un évènement
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "message": "creneau deleted"
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Créneau introuvable"
 *     }
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Suppression impossible (Participation)"
 *     }
 * @apiError (401 Non autorisé) Unhautorized Le créneau n'existe pas
 * @apiErrorExample Réponse (exemple):
 *     401 Non autorisé
 *     {
 *       "message: "Not authorized"
 *     }
 */
creneauRoute.delete('/:id', creneauController.deleteCreneau);

/**
 * @api {get} /evenements/:id Récupérer les créneaux
 * @apiGroup Creneau
 * @apiPermission participant
 * @apiDescription Permet de récupérer les créneaux d'un évènement
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {Date}   dateDebut   Date de début
 * @apiSuccess {Int}   participant   Nombre de participants autorisés
 * @apiSuccess {Int}   nbParticipant   Nombre de participants présents
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "id_": result._id
 *          "dateDebut": result.dateDebut
 *          "participant": result.participant
 *          "nbParticipant": result.nbParticipant
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Evènement ou créneau introuvable"
 *     }
 */
creneauRoute.get('/evenements/:id', creneauController.getCreneauxByEv);

module.exports = creneauRoute;

