const participationController = require('../controllers/participationController.js');
const express = require('express');
const participationRoute = express.Router();

/**
 * @api {post} /participations/ Ajouter une participation
 * @apiGroup Participation
 * @apiPermission participant
 * @apiDescription Permet d'ajouter une particpation à un créneau
 * @apiSuccess {ObjectId}   id_   Identifiant
 * @apiSuccess {ObjectId}   idUtilisateur   Identifiant de l'utilisateur
 * @apiSuccess {ObjectId}   idEvenement   Identifiant de l'évènement
 * @apiSuccess {ObjectId}   idSlot   Identifiant du créneau
 * @apiSuccessExample {json} Exemple de succès
 *      201 Created
 *      {
 *          "id": result._id
 *          "idUtilisateur": result.idUtilisateur
 *          "idEvenement": result.idEvenement
 *          "idSlot": result.idSlot
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "Erreur ajout"
 *     }
 */
participationRoute.post('/', participationController.addParticipation);

/**
 * @api {delete} /participations/:id Supprimer une participation
 * @apiGroup Participation
 * @apiPermission organisateur
 * @apiDescription Permet de supprimer un créneau d'un évènement
 * @apiSuccessExample {json} Exemple de succès
 *      200 OK
 *      {
 *          "_id": result._id
 *      }
 * @apiError (500 Erreur interne du serveur) InternalServerError Le serveur a rencontré une erreur interne
 * @apiErrorExample Réponse (exemple):
 *     500 Erreur interne du serveur
 *     {
 *       "message": "erreur de suppression"
 *     }
 */
participationRoute.delete('/:id', participationController.deleteParticipation);

module.exports = participationRoute;

