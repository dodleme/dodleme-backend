const express = require('express');
const app = express();
const mongoose = require('mongoose');
const utilisateurRoute = require('./routes/utilisateurRoute');
const evenementRoute = require('./routes/evenementRoute');
const creneauRoute = require('./routes/creneauRoute');
const participationRoute = require('./routes/participationRoute');
const notificationRoute = require('./routes/notificationRoute');
const authRoute = require('./routes/authRoute')
const bodyParser = require('body-parser');

//Permet au serveur de retourner du json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Permet de définir l'accès et l'utilisation de certaines méthodes.
// si jamais on rencontre des problemes avec des headers on va use cors

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    next();
});


mongoose.connect("mongodb://localhost:27017/DodleMe", { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
    .then(() => {
      console.log('Connected to database');
    })
    .catch(() => {
      console.log('Connection failed');
    });

//Attachement de la route utilisateur à l'appli express
app.use('/auth', authRoute);
app.use('/evenements', evenementRoute);
app.use('/utilisateurs', utilisateurRoute);
app.use('/creneaux', creneauRoute);
app.use('/participations', participationRoute);
app.use('/notifications', notificationRoute);
//Initialisation du serveur express
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
