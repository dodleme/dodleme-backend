const mongoose = require('mongoose');
/**
 * Model des participation d'un utilisateur
 */
const participationSchema = mongoose.Schema(
    {
        idUtilisateur: { type: mongoose.Schema.ObjectId, ref: 'Utilisateur', required: true},
        idSlot: { type: mongoose.Schema.ObjectId, ref: 'Slot', required: true }
    }
);

module.exports = mongoose.model('Participation', participationSchema);
