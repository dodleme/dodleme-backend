const mongoose = require('mongoose');
const schema = mongoose.Schema;
// Creation du model notification
/**
 * Cet objet définira la structure d'une notification
 */
const notificationSchema = mongoose.Schema({
    idUtilisateur: {type: schema.ObjectId, ref: 'Utilisateur', required: true},
    idEvenement: {type: schema.ObjectId, ref: 'Evenement', required: true},
    titre: {type: String, required: true},
    message: {type: String, required: true},
    lu: {type:Boolean, required: true}
});

module.exports = mongoose.model('Notification', notificationSchema);
