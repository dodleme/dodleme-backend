
const mongoose = require('mongoose');
// Creation du model utilisateur
/**
 * Cet objet définira aussi la structure de la table Utilisateur si on fait le parallele avec 
 * les BDD relationnels
 */
const utilisateurSchema = mongoose.Schema({
  identifiant: { type: String, required: true, unique: true },
  nom : {type: String, required: true}, // définition du type de l'attribut et s'il est requis
  prenom: { type: String, required: true },
  password: {type: String, required: true}
});


module.exports = mongoose.model('Utilisateur', utilisateurSchema);
