const mongoose = require('mongoose');
const schema = mongoose.Schema;

const slotSchema = mongoose.Schema(
    {
        idEvenement: {type: schema.ObjectId, reference: 'Evenement', required: true},
        dateDebut: {type: Date, required: true}
    }
);

module.exports = mongoose.model('Slot', slotSchema);
