const mongoose = require('mongoose');
const schema = mongoose.Schema;
const evenementSchema = mongoose.Schema({
        nom: {type: String, required: true},
        duree: {type: Number, required: true},
        description: {type: String, required: false},
        participant: {type: Number, required: true},
        etat: {type:Number, required: true},
        idCreneauFinal: {type: schema.ObjectId, ref: 'Slot', required: false},
        idUtilisateur: {type: schema.ObjectId, ref: 'Utilisateur', required: true},
        creneaux: [{type: schema.ObjectId, ref: 'Slot', required: false}],
}); // On peut créer des id qui font reference à un autre modele

module.exports = mongoose.model('Evenement', evenementSchema);
