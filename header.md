La documentation de l'API est générée par Apidoc. Pour lire correctement le fichier index.html, il faut l'ouvrir depuis le projet car ce fichier fait appel à d'autres fichiers pour se construire.
Cependant, une version en PDF dans le projet est disponible mais elle n'est pas aussi bien généré car le formatage n'a pas tout inclut.
